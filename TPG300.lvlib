﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="18008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.Lib.Description" Type="Str">LabVIEW Plug and Play instrument driver for &lt;b&gt;Total pressure gauge and controller TPG300&lt;/b&gt; of &lt;b&gt;balzers Instruments&lt;/b&gt;.

Lizenziert unter EUPL V. 1.1

Copyright 2012 GSI Helmholtzzentrum für Schwerionenforschung GmbH

Dr. Holger Brand, EE/KS
Planckstr. 1, 64291 Darmstadt
Germany

In case of questions send e-mail to H.Brand@gsi.de.</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*#!!!*Q(C=\:7R41."%%5`#!E)(:#$*34S;=%./(!$"./#5U+\B!FIQ#WY!!+G"&lt;@A&amp;IZX?Y.N!=)%))(%LM&gt;X^W&gt;WZNXO?CWV&gt;C'&gt;K4P72J_WL`D\*%/CY;[`+&gt;M^N5`F[H&lt;^Q.].%@P1.`\2`PLB_+*Z8V_@NH``P`^P_Y]U9B[/"PVQEUY;+::99)Z:?\MOU2-^U2-^U2-^U!-^U!-^U!0&gt;U2X&gt;U2X&gt;U2X&gt;U!X&gt;U!X&gt;U!W^&gt;83B#VXIT%J30#G5*%U3*-'A+0F+?"+?B#@BY6%*4]+4]#1]#1]B3HA3HI1HY5FY'+;%*_&amp;*?"+?B)&gt;5D32&lt;2Y=HY3'^!J[!*_!*?!)?3CLA#1##9E(C)!E9#JT"4=!4]!1]X#LA#8A#HI!HY-'NA#@A#8A#HI#()7V7IN(U(2U?UMDB=8A=(I@(Y3'V("[(R_&amp;R?"Q?SMHB=8A=#+?AERQ%/9/=!/@"Y8&amp;YO-DB=8A=(I@(Y=(66MD&lt;T01U@5?(R_!R?!Q?A]@A)95-(I0(Y$&amp;Y$"\3SO!R?!Q?A]@AI:1-(I0(Y$&amp;!D++5FZ(-''A%'9,"Q[@N&amp;GOL&amp;)X%7K_@ZL"262N1N&lt;&amp;5'U;V%61,L&amp;IYV9+I*FIVA;K*5&lt;WQ[E65A+L#KI3K1'XZXG"L&lt;)5NM$EWQS&lt;9'"PV1\]Z=,P&gt;;L0:;,V?;\6;;&lt;&amp;9;$[@;T;&lt;;4+:;$Q?CT`BX7FV42`;S?OZ^-"^&lt;Y^0^T@,O[PJ]PFWOLTE?HZ`]_L\#2P/J&gt;^Q.OJ5X=%RTRS^!&amp;."/AM!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">402685952</Property>
	<Property Name="NI.Lib.Version" Type="Str">2.0.1.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="Examples" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Read Pressures Example.vi" Type="VI" URL="../Examples/Read Pressures Example.vi"/>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Default Instrument Setup.vi" Type="VI" URL="../Private/Default Instrument Setup.vi"/>
		<Item Name="Transaction.vi" Type="VI" URL="../Private/Transaction.vi"/>
	</Item>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Action-Status" Type="Folder">
			<Item Name="Setpoint Status.vi" Type="VI" URL="../Public/Action-Status/Setpoint Status.vi"/>
			<Item Name="Store Parameter.vi" Type="VI" URL="../Public/Action-Status/Store Parameter.vi"/>
			<Item Name="Authorized Access.vi" Type="VI" URL="../Public/Action-Status/Authorized Access.vi"/>
		</Item>
		<Item Name="Configure" Type="Folder">
			<Item Name="Filter Time.vi" Type="VI" URL="../Public/Configure/Filter Time.vi"/>
			<Item Name="Unit.vi" Type="VI" URL="../Public/Configure/Unit.vi"/>
			<Item Name="PE Underrange Control.vi" Type="VI" URL="../Public/Configure/PE Underrange Control.vi"/>
			<Item Name="Threshold Value.vi" Type="VI" URL="../Public/Configure/Threshold Value.vi"/>
			<Item Name="Threshold Pressure.vi" Type="VI" URL="../Public/Configure/Threshold Pressure.vi"/>
			<Item Name="Measuring Circuit Mode.vi" Type="VI" URL="../Public/Configure/Measuring Circuit Mode.vi"/>
		</Item>
		<Item Name="Data" Type="Folder">
			<Item Name="Read Measurement.vi" Type="VI" URL="../Public/Data/Read Measurement.vi"/>
			<Item Name="Read Pressure.vi" Type="VI" URL="../Public/Data/Read Pressure.vi"/>
		</Item>
		<Item Name="Utility" Type="Folder">
			<Item Name="Error Query.vi" Type="VI" URL="../Public/Utility/Error Query.vi"/>
			<Item Name="Revision Query.vi" Type="VI" URL="../Public/Utility/Revision Query.vi"/>
		</Item>
		<Item Name="Typedefs" Type="Folder">
			<Item Name="Measuring Circuit.ctl" Type="VI" URL="../Public/Typedefs/Measuring Circuit.ctl"/>
		</Item>
		<Item Name="Close.vi" Type="VI" URL="../Public/Close.vi"/>
		<Item Name="Initialize.vi" Type="VI" URL="../Public/Initialize.vi"/>
		<Item Name="VI Tree.vi" Type="VI" URL="../Public/VI Tree.vi"/>
	</Item>
	<Item Name="Balzers TPG300.pdf" Type="Document" URL="../Balzers TPG300.pdf"/>
	<Item Name="EUPL v.1.1 - Lizenz.pdf" Type="Document" URL="../EUPL v.1.1 - Lizenz.pdf"/>
	<Item Name="EUPL v.1.1 - Lizenz.rtf" Type="Document" URL="../EUPL v.1.1 - Lizenz.rtf"/>
	<Item Name="Test Example.vi" Type="VI" URL="../Examples/Test Example.vi"/>
	<Item Name="TPG300 Readme.html" Type="Document" URL="../TPG300 Readme.html"/>
</Library>
